#include <stdio.h>
#include <string.h>
#include <ctype.h>

int split(char *str, char *first_string, char *second_string) {
    strtok(str, "\n");
    char *pch = strtok(str, " ");
    strcpy(first_string, pch);
    pch = strtok(NULL, " ");
    strcpy(second_string, pch);
}

char *matchString(char *string, char *mask) {
    size_t string_lenght = strlen(string);
    for (int i = 0; i < string_lenght; i++) {
        if (mask[i] == '1') {
            string[i] = (char) toupper(string[i]);
        }
        if (mask[i] == '0') {
            string[i] = (char) tolower(string[i]);
        }
    }
    return string;
}

int main(int argc, const char *argv[]) {
    FILE *file = fopen(argv[1], "r");
    char line[1024];
    char first_string[512];
    char second_string[512];
    while (fgets(line, 1024, file)) {
        split(line, first_string, second_string);
        printf("%s\n", matchString(first_string, second_string));
    }
    return 0;
}